﻿// FADForm.cs

using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using TimeManager;
using NodaTime;

namespace AmazonAppOfTheDay
{
    public partial class FADForm : Form
    {
        #region Private Member Variables
        private bool m_CheckIsRunning = false; // Amazon FAD Checker Running?
        private TimeManager.Alarm m_Alarm = null;
        #endregion

        #region Constructors
        public FADForm()
        {
            InitializeComponent();
            m_Alarm = new TimeManager.Alarm();
            m_Alarm.TargetTimeReached += new TimeManager.Alarm.TargetTimeReachedEventHandler(Alarm_TargetTimeReached);
            m_Alarm.TimerInterrupted += new TimeManager.Alarm.TimerInterruptedEventHandler(Alarm_TimerInterrupted);

            m_BackgroundWorker.WorkerSupportsCancellation = true;
            m_BackgroundWorker.DoWork += new DoWorkEventHandler(BackgroundWorker_DoWork);
            m_BackgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BackgroundWorker_RunWorkerCompleted);
            
            m_FlashBackgroundWorker.WorkerSupportsCancellation = true;
            m_FlashBackgroundWorker.DoWork += new DoWorkEventHandler(FlashBackgroundWorker_DoWork);
            m_FlashBackgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(FlashBackgroundWorker_RunWorkerCompleted);

            m_NotifyIcon.Visible = false;
            m_NotifyIcon.Icon = this.Icon;
            m_NotifyIcon.ContextMenuStrip = m_ContextMenuStrip;
            m_NotifyIcon.MouseDoubleClick += new MouseEventHandler(NotifyIcon_MouseDoubleClick);
            m_NotifyIcon.BalloonTipClosed += new EventHandler(NotifyIcon_BalloonTipClosed);
            m_NotifyIcon.BalloonTipClicked += new EventHandler(NotifyIcon_BalloonTipClicked);

            m_OpenMenuItem.Click += new EventHandler(OpenMenuItem_Click);
            m_ViewLastPurchaseMenuItem.Click += new EventHandler(ViewLastPurchaseMenuItem_Click);
            m_CheckNowMenuItem.Click += new EventHandler(CheckNowMenuItem_Click);
            m_QuitMenuItem.Click += new EventHandler(QuitMenuItem_Click);

            m_TimeSelectComboBox.SelectedIndex = m_TimeSelectComboBox.Items.Count - 1;
            m_TimeSelectComboBox.SelectedIndexChanged += new EventHandler(TimeSelectComboBox_SelectedIndexChanged);

            this.Resize += new EventHandler(Form_Resize);

            m_NotifyIcon.Text = Strings.NotifyTitle_Minimized;
            m_CheckNowButton.Text = Strings.CmdCheckNow;
            m_EmailLabel.Text = Strings.InputDesc_EmailAddress;
            m_PasswordLabel.Text = Strings.InputDesc_Password;
            m_InfoLabel.Text = Strings.AppDescription;
            m_OpenMenuItem.Text = Strings.CmdOpen;
            m_ViewLastPurchaseMenuItem.Text = Strings.CmdViewLastPurchase;
            m_CheckNowMenuItem.Text = Strings.CmdCheckNow;
            m_QuitMenuItem.Text = Strings.CmdQuit;
            m_TimeSelectLabel.Text = Strings.InputDesc_TimeSelect;
            this.Text = Strings.AppTitle;

            UpdateAlarm();
        }
        #endregion

        #region Private Functions
        private void CheckAmazonNow()
        {
            if (!m_CheckIsRunning)
            {
                bool checkAllowed = true;
                if (String.IsNullOrEmpty(m_EmailTextBox.Text))
                {
                    m_EmailTextBox.BackColor = Color.Red;
                    checkAllowed = false;
                }
                if (String.IsNullOrEmpty(m_PasswordTextBox.Text))
                {
                    m_PasswordTextBox.BackColor = Color.Red;
                    checkAllowed = false;
                }
                if (checkAllowed)
                {
                    m_CheckIsRunning = true;
                    m_CheckNowButton.Enabled = false;
                    m_BackgroundWorker.RunWorkerAsync();
                }
                else
                {
                    m_FlashBackgroundWorker.RunWorkerAsync();
                    DisplayBalloonTip(Strings.NotifyTitle_CheckFailed, Strings.NotifyDesc_CheckFailedDueToInvalidCredentials);
                    
                }
                return;
            }
            DisplayBalloonTip(Strings.NotifyTitle_CheckFailed, Strings.NotifyDesc_CheckFailedProcessAlreadyRunning);
        }
        private void DisplayBalloonTip(string title, string text)
        {
            m_NotifyIcon.BalloonTipTitle = title;
            m_NotifyIcon.BalloonTipText = text;
            m_NotifyIcon.Visible = true;
            m_NotifyIcon.ShowBalloonTip(1);
        }
        private void ViewLastPurchaseFailed()
        {
            DisplayBalloonTip(Strings.NotifyTitle_ViewLastPurchaseFailed, Strings.NotifyDesc_AppAcquiredCouldNotRetrieveData);
        }
        /// <summary>
        /// Display notification signifying a FAD has been purchased
        /// </summary>
        private void NotifyAppPurchaseComplete()
        {
            string appName = AmazonRequest.GetLastPurchaseData(AmazonRequest.APP_NAME);
            if (appName == null)
            {
                appName = Strings.NotifyDesc_AppAcquiredCouldNotRetrieveData;
            }
            DisplayBalloonTip(Strings.NotifyTitle_AppAcquired, appName);

        }
        /// <summary>
        /// TODO: Updates label on form displaying last purchase date and time
        /// </summary>
        private void UpdateLastPurchaseTime()
        {
            m_LastPurchaseTimeLabel.Text = String.Empty;
        }
        /// <summary>
        /// Updates Alarm with a new Target Time using the Dropdown Box selection
        /// </summary>
        private void UpdateAlarm()
        {
            NodaTime.Text.LocalTimePattern selectedHourPattern = NodaTime.Text.LocalTimePattern.CreateWithInvariantCulture("H:mm");
            NodaTime.LocalTime selectedHour = selectedHourPattern.Parse(m_TimeSelectComboBox.SelectedItem.ToString()).Value;
            NodaTime.ZonedDateTime systemTime = TimeManager.TimeInfo.GetSystemTime();
            NodaTime.ZonedDateTime targetTime = new ZonedDateTime(); // default assignment
            int hourDiff = systemTime.Hour - selectedHour.Hour;
            if (hourDiff > -24 && hourDiff < 0) // Stay same
            {
                targetTime = GetSystemTimeWithTargetHour(systemTime, selectedHour.Hour);
            }
            else if (hourDiff >= 0 && hourDiff < 12) // Add 12 hours
            {
                targetTime = GetSystemTimeWithTargetHour(systemTime, selectedHour.Hour);
                targetTime = targetTime.Plus(Duration.FromHours(12));
            }
            else if (hourDiff >= 12 && hourDiff < 24) // Add 24 hours
            {
                targetTime = GetSystemTimeWithTargetHour(systemTime, selectedHour.Hour);
                targetTime = targetTime.Plus(Duration.FromHours(24));
            }

            m_Alarm.SetTargetTime(targetTime);
        }
        /// <summary>
        /// Gets the System Time, but replaces the hour with the specified hour
        /// </summary>
        private NodaTime.ZonedDateTime GetSystemTimeWithTargetHour(ZonedDateTime systemTime, int halfDayHour)
        {
            return new NodaTime.LocalDateTime(systemTime.Year, systemTime.Month, systemTime.Day, halfDayHour, 0, systemTime.Calendar).InZoneStrictly(systemTime.Zone);
        }
        private void MinimizeToSystemTray()
        {
            this.ShowInTaskbar = false;
            this.Hide();
            DisplayBalloonTip(Strings.NotifyTitle_Minimized, Strings.NotifyDesc_RunningInBackground);
        }
        private void HideNotifyIcon()
        {
            if (this.Visible) // Only hide NotifyIcon if Form is visible
            {
                m_NotifyIcon.Visible = false;
            }
        }
        private void ShowApp()
        {
            if (WindowState == FormWindowState.Minimized) // If Form was minimized, show it now
            {
                WindowState = FormWindowState.Normal;
                m_NotifyIcon.Visible = false;
                this.ShowInTaskbar = true;
                this.Show();
            }
        }
        #endregion

        #region BackgroundWorker EventHandlers
        private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (AmazonRequest.AttemptFreeAppPurchase(m_EmailTextBox.Text, m_PasswordTextBox.Text))
            {
                NotifyAppPurchaseComplete();
            }
        }
        private void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_CheckIsRunning = false;
            m_CheckNowButton.Enabled = true;
        }
        private void FlashBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Threading.Thread.Sleep(50); // Pause to cause background to have a flash effect
        }
        private void FlashBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            m_EmailTextBox.BackColor = Color.White;
            m_PasswordTextBox.BackColor = Color.White;
        }
        #endregion

        #region Alarm EventHandlers
        private void Alarm_TargetTimeReached(object sender, TimeManager.TargetTimeReachedArgs e)
        {
            CheckAmazonNow();
            UpdateAlarm();
        }
        private void Alarm_TimerInterrupted(object sender, TimeManager.TimerInterruptedArgs e)
        {
            UpdateAlarm();
        }
        #endregion

        #region Form EventHandlers
        private void Form_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                MinimizeToSystemTray();
            }
        }
        private void TimeSelectComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateAlarm();
        }
        private void CheckNowButton_Click(object sender, EventArgs e)
        {
            CheckAmazonNow();
        }
        #endregion

        #region MenuItem EventHandlers
        private void OpenMenuItem_Click(object sender, EventArgs e)
        {
            ShowApp();
        }
        private void ViewLastPurchaseMenuItem_Click(object sender, EventArgs e)
        {
            string appLink = AmazonRequest.GetLastPurchaseData(AmazonRequest.APP_LINK);
            if (appLink != null)
            {
                try
                {
                    Process.Start(appLink); // Load webpage for last FAD purchased
                    return;
                }
                catch (InvalidOperationException)
                {

                }
                catch (Win32Exception)
                {

                }
            }

            ViewLastPurchaseFailed();
        }
        private void CheckNowMenuItem_Click(object sender, EventArgs e)
        {
            CheckAmazonNow();
        }
        private void QuitMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        #endregion

        #region NotifyIcon EventHandlers
        private void NotifyIcon_BalloonTipClicked(object sender, EventArgs e)
        {
            HideNotifyIcon();
        }
        private void NotifyIcon_BalloonTipClosed(object sender, EventArgs e)
        {
            HideNotifyIcon();
        }
        private void NotifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ShowApp();
        }
        #endregion
    }
}
