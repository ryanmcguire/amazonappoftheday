﻿// Localization.cs

using System.Threading;
using System.Globalization;

namespace AmazonAppOfTheDay.Util
{
    static class Localization
    {
        #region Constructors
        static Localization()
        {

        }
        #endregion

        #region Public Member Functions
        public static void SetCurrentThreadLanguage(LOCALE_ID id)
        {
            string language = string.Empty;
            switch (id)
            {
                // US English (Default)
                default:
                case LOCALE_ID.ENGLISH_US:
                    language = "en-US";
                    break;
            }

            CultureInfo cultureInfo = new CultureInfo(language, true);
            Thread currentThread = Thread.CurrentThread;
            currentThread.CurrentUICulture = cultureInfo;
        }
        #endregion
    }
}
