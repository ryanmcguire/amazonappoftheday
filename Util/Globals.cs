﻿// Globals.cs

namespace AmazonAppOfTheDay.Util
{
    #region Languages Enum
    public enum LOCALE_ID
    {
        INVALID = 0x00,
        ENGLISH_US = 0x0409,
    }
    #endregion

    static class Globals
    {
        #region Constructors
        static Globals()
        {

        }
        #endregion
    }
}
