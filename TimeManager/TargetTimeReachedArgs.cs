﻿// TargetTimeReachedArgs.cs

using NodaTime;

namespace TimeManager
{
    public class TargetTimeReachedArgs : TimerInterruptedArgs
    {
        #region Constructors
        public TargetTimeReachedArgs(NodaTime.ZonedDateTime targetTime, NodaTime.ZonedDateTime eventPostedTime) : base(eventPostedTime)
        {
            TargetTime = targetTime;
        }
        #endregion

        #region Public Accessors
        public NodaTime.ZonedDateTime TargetTime { get; private set; }
        #endregion
    }
}
