﻿// TimerInterruptedArgs.cs

using NodaTime;

namespace TimeManager
{
    public class TimerInterruptedArgs
    {
        #region Constructors
        public TimerInterruptedArgs(NodaTime.ZonedDateTime eventPostedTime)
        {
            EventPostedTime = eventPostedTime;
            SystemTimeZone = NodaTime.DateTimeZoneProviders.Tzdb.GetSystemDefault();
        }
        #endregion

        #region Public Accessors
        public NodaTime.ZonedDateTime EventPostedTime { get; private set; }
        public NodaTime.DateTimeZone SystemTimeZone { get; private set; }
        #endregion
    }
}
