﻿// TimeInfo.cs

using NodaTime;

namespace TimeManager
{
    static class TimeInfo
    {
        #region Constructors
        static TimeInfo()
        {

        }
        #endregion

        #region Public Member Functions
        public static NodaTime.ZonedDateTime GetSystemTime()
        {
            return NodaTime.SystemClock.Instance.Now.InZone(NodaTime.DateTimeZoneProviders.Tzdb.GetSystemDefault());
        }
        public static NodaTime.ZonedDateTime GetSystemTimeInZone(NodaTime.DateTimeZone zone)
        {
            return NodaTime.SystemClock.Instance.Now.InZone(zone);
        }
        #endregion
    }
}
