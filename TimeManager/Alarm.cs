﻿// Alarm.cs

using System;
using System.Text;
using Microsoft.Win32;
using System.Windows.Forms;
using NodaTime;

namespace TimeManager
{
    
    public class Alarm : IDisposable
    {
        #region Private Member Variables
        private bool m_StartedBeforeTargetTime = false;
        private NodaTime.ZonedDateTime m_TargetTime;
        private Timer m_Timer = null;
        #endregion

        #region Event Signatures/Declarations
        public delegate void TargetTimeReachedEventHandler(object sender, TargetTimeReachedArgs e);
        public delegate void TimerInterruptedEventHandler(object sender, TimerInterruptedArgs e);
        public event TargetTimeReachedEventHandler TargetTimeReached = delegate { };
        public event TimerInterruptedEventHandler TimerInterrupted = delegate { };
        #endregion

        #region Constructors
        public Alarm()
        {
            m_Timer = new Timer();
            m_Timer.Tick += new EventHandler(Timer_Tick);
            
            SystemEvents.TimeChanged += new EventHandler(SystemEvents_TimeChanged);
        }
        #endregion

        #region Private Member Functions
        private bool UpdateTimer()
        {
            m_Timer.Enabled = false;
            if (TargetTimeIsValid())
            {
                m_StartedBeforeTargetTime = true;
                RenewTimerInterval();
                return true;
            }
            else if (m_StartedBeforeTargetTime)
            {
                m_StartedBeforeTargetTime = false;
                OnTargetTimeReached(new TargetTimeReachedArgs(m_TargetTime, TimeInfo.GetSystemTimeInZone(m_TargetTime.Zone)));
            }
            else
            {
                OnTimerInterrupted(new TimerInterruptedArgs(TimeInfo.GetSystemTimeInZone(m_TargetTime.Zone)));
            }
            return false;
        }
        private void RenewTimerInterval()
        {
            int wait = (int)NodaTime.Period.Between(TimeInfo.GetSystemTimeInZone(m_TargetTime.Zone).LocalDateTime,
                m_TargetTime.LocalDateTime, NodaTime.PeriodUnits.Milliseconds).Milliseconds;
            m_Timer.Interval = (wait == 0) ? 1 : wait; // If the timing is really close, lean in favor of it being a valid target time
            m_Timer.Enabled = true;
        }
        private bool TargetTimeIsValid()
        {
            return TimeInfo.GetSystemTimeInZone(m_TargetTime.Zone).CompareTo(m_TargetTime) < 0;
        }
        #endregion

        #region Public Member Functions
        public bool SetTargetTime(NodaTime.ZonedDateTime targetTime)
        {
            m_TargetTime = targetTime;
            m_StartedBeforeTargetTime = false;
            return UpdateTimer();
        }
        #endregion

        #region Timer EventHandlers
        private void Timer_Tick(object sender, EventArgs e)
        {
            UpdateTimer();
        }
        #endregion

        #region Event Functions
        protected void OnTargetTimeReached(TargetTimeReachedArgs e)
        {
            TargetTimeReached(this, e);
        }
        protected void OnTimerInterrupted(TimerInterruptedArgs e)
        {
            TimerInterrupted(this, e);
        }
        #endregion

        #region System EventHandlers
        private void SystemEvents_TimeChanged(object sender, EventArgs e)
        {
            UpdateTimer();
        }
        #endregion

        #region IDisposable Implementation
        public void Dispose()
        {
            m_Timer.Stop();
            m_Timer.Tick -= Timer_Tick;
            m_Timer.Dispose();
            // Because this is a static event, you must detach your event handlers when your application is disposed, or memory leaks will result.
            SystemEvents.TimeChanged -= SystemEvents_TimeChanged;
        }
        #endregion
    }
}
