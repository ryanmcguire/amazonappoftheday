﻿namespace AmazonAppOfTheDay
{
    partial class FADForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FADForm));
            this.m_NotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.m_CheckNowButton = new System.Windows.Forms.Button();
            this.m_EmailLabel = new System.Windows.Forms.Label();
            this.m_PasswordLabel = new System.Windows.Forms.Label();
            this.m_InfoLabel = new System.Windows.Forms.Label();
            this.m_EmailTextBox = new System.Windows.Forms.TextBox();
            this.m_PasswordTextBox = new System.Windows.Forms.TextBox();
            this.m_BackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.m_FlashBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.m_ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_OpenMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ViewLastPurchaseMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.m_CheckNowMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.m_QuitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_TimeSelectLabel = new System.Windows.Forms.Label();
            this.m_TimeSelectComboBox = new System.Windows.Forms.ComboBox();
            this.m_LastPurchaseTimeLabel = new System.Windows.Forms.Label();
            this.m_ContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_NotifyIcon
            // 
            this.m_NotifyIcon.Visible = true;
            // 
            // m_CheckNowButton
            // 
            this.m_CheckNowButton.Location = new System.Drawing.Point(201, 141);
            this.m_CheckNowButton.Name = "m_CheckNowButton";
            this.m_CheckNowButton.Size = new System.Drawing.Size(71, 23);
            this.m_CheckNowButton.TabIndex = 5;
            this.m_CheckNowButton.UseVisualStyleBackColor = true;
            this.m_CheckNowButton.Click += new System.EventHandler(this.CheckNowButton_Click);
            // 
            // m_EmailLabel
            // 
            this.m_EmailLabel.AutoSize = true;
            this.m_EmailLabel.Location = new System.Drawing.Point(12, 61);
            this.m_EmailLabel.Name = "m_EmailLabel";
            this.m_EmailLabel.Size = new System.Drawing.Size(79, 13);
            this.m_EmailLabel.TabIndex = 1;
            // 
            // m_PasswordLabel
            // 
            this.m_PasswordLabel.AutoSize = true;
            this.m_PasswordLabel.Location = new System.Drawing.Point(12, 89);
            this.m_PasswordLabel.Name = "m_PasswordLabel";
            this.m_PasswordLabel.Size = new System.Drawing.Size(56, 13);
            this.m_PasswordLabel.TabIndex = 3;
            // 
            // m_InfoLabel
            // 
            this.m_InfoLabel.Location = new System.Drawing.Point(12, 10);
            this.m_InfoLabel.Name = "m_InfoLabel";
            this.m_InfoLabel.Size = new System.Drawing.Size(260, 40);
            this.m_InfoLabel.TabIndex = 0;
            // 
            // m_EmailTextBox
            // 
            this.m_EmailTextBox.Location = new System.Drawing.Point(97, 58);
            this.m_EmailTextBox.Name = "m_EmailTextBox";
            this.m_EmailTextBox.Size = new System.Drawing.Size(175, 20);
            this.m_EmailTextBox.TabIndex = 2;
            this.m_EmailTextBox.WordWrap = false;
            // 
            // m_PasswordTextBox
            // 
            this.m_PasswordTextBox.Location = new System.Drawing.Point(97, 86);
            this.m_PasswordTextBox.Name = "m_PasswordTextBox";
            this.m_PasswordTextBox.PasswordChar = '*';
            this.m_PasswordTextBox.Size = new System.Drawing.Size(175, 20);
            this.m_PasswordTextBox.TabIndex = 4;
            this.m_PasswordTextBox.WordWrap = false;
            // 
            // m_ContextMenuStrip
            // 
            this.m_ContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_OpenMenuItem,
            this.m_ViewLastPurchaseMenuItem,
            this.m_ToolStripSeparator1,
            this.m_CheckNowMenuItem,
            this.m_ToolStripSeparator2,
            this.m_QuitMenuItem});
            this.m_ContextMenuStrip.Name = "m_ContextMenuStrip";
            this.m_ContextMenuStrip.Size = new System.Drawing.Size(175, 104);
            // 
            // m_OpenMenuItem
            // 
            this.m_OpenMenuItem.Name = "m_OpenMenuItem";
            this.m_OpenMenuItem.Size = new System.Drawing.Size(174, 22);
            // 
            // m_ViewLastPurchaseMenuItem
            // 
            this.m_ViewLastPurchaseMenuItem.Name = "m_ViewLastPurchaseMenuItem";
            this.m_ViewLastPurchaseMenuItem.Size = new System.Drawing.Size(174, 22);
            // 
            // m_ToolStripSeparator1
            // 
            this.m_ToolStripSeparator1.Name = "m_ToolStripSeparator1";
            this.m_ToolStripSeparator1.Size = new System.Drawing.Size(171, 6);
            // 
            // m_CheckNowMenuItem
            // 
            this.m_CheckNowMenuItem.Name = "m_CheckNowMenuItem";
            this.m_CheckNowMenuItem.Size = new System.Drawing.Size(174, 22);
            // 
            // m_ToolStripSeparator2
            // 
            this.m_ToolStripSeparator2.Name = "m_ToolStripSeparator2";
            this.m_ToolStripSeparator2.Size = new System.Drawing.Size(171, 6);
            // 
            // m_QuitMenuItem
            // 
            this.m_QuitMenuItem.Name = "m_QuitMenuItem";
            this.m_QuitMenuItem.Size = new System.Drawing.Size(174, 22);
            // 
            // m_TimeSelectLabel
            // 
            this.m_TimeSelectLabel.AutoSize = true;
            this.m_TimeSelectLabel.Location = new System.Drawing.Point(12, 117);
            this.m_TimeSelectLabel.Name = "m_TimeSelectLabel";
            this.m_TimeSelectLabel.Size = new System.Drawing.Size(162, 13);
            this.m_TimeSelectLabel.TabIndex = 6;
            // 
            // m_TimeSelectComboBox
            // 
            this.m_TimeSelectComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_TimeSelectComboBox.FormattingEnabled = true;
            this.m_TimeSelectComboBox.Items.AddRange(new object[] {
            "1:00",
            "2:00",
            "3:00",
            "4:00",
            "5:00",
            "6:00",
            "7:00",
            "8:00",
            "9:00",
            "10:00",
            "11:00",
            "12:00"});
            this.m_TimeSelectComboBox.Location = new System.Drawing.Point(183, 114);
            this.m_TimeSelectComboBox.Name = "m_TimeSelectComboBox";
            this.m_TimeSelectComboBox.Size = new System.Drawing.Size(89, 21);
            this.m_TimeSelectComboBox.TabIndex = 7;
            // 
            // m_LastPurchaseTimeLabel
            // 
            this.m_LastPurchaseTimeLabel.AutoSize = true;
            this.m_LastPurchaseTimeLabel.Location = new System.Drawing.Point(15, 146);
            this.m_LastPurchaseTimeLabel.Name = "m_LastPurchaseTimeLabel";
            this.m_LastPurchaseTimeLabel.Size = new System.Drawing.Size(0, 13);
            this.m_LastPurchaseTimeLabel.TabIndex = 8;
            // 
            // FADForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 173);
            this.Controls.Add(this.m_LastPurchaseTimeLabel);
            this.Controls.Add(this.m_TimeSelectComboBox);
            this.Controls.Add(this.m_TimeSelectLabel);
            this.Controls.Add(this.m_PasswordTextBox);
            this.Controls.Add(this.m_EmailTextBox);
            this.Controls.Add(this.m_InfoLabel);
            this.Controls.Add(this.m_PasswordLabel);
            this.Controls.Add(this.m_EmailLabel);
            this.Controls.Add(this.m_CheckNowButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FADForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.m_ContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon m_NotifyIcon;
        private System.Windows.Forms.Button m_CheckNowButton;
        private System.Windows.Forms.Label m_EmailLabel;
        private System.Windows.Forms.Label m_PasswordLabel;
        private System.Windows.Forms.Label m_InfoLabel;
        private System.Windows.Forms.TextBox m_EmailTextBox;
        private System.Windows.Forms.TextBox m_PasswordTextBox;
        private System.ComponentModel.BackgroundWorker m_BackgroundWorker;
        private System.ComponentModel.BackgroundWorker m_FlashBackgroundWorker;
        private System.Windows.Forms.ContextMenuStrip m_ContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem m_CheckNowMenuItem;
        private System.Windows.Forms.ToolStripSeparator m_ToolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem m_QuitMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_OpenMenuItem;
        private System.Windows.Forms.ToolStripSeparator m_ToolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem m_ViewLastPurchaseMenuItem;
        private System.Windows.Forms.Label m_TimeSelectLabel;
        private System.Windows.Forms.ComboBox m_TimeSelectComboBox;
        private System.Windows.Forms.Label m_LastPurchaseTimeLabel;
    }
}

