﻿// Program.cs

using System;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using AmazonAppOfTheDay.Util;
using System.Globalization;

namespace AmazonAppOfTheDay
{
    static class Program
    {

        #region Constructors
        static Program()
        {
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
        }
        #endregion

        #region Main Method
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Localization.SetCurrentThreadLanguage((LOCALE_ID)CultureInfo.InstalledUICulture.LCID); // try to use installed language
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FADForm());
        }
        #endregion

        #region Private Member Functions
        /// <summary>
        /// Loads embedded .dll resources in the "lib" folder
        /// </summary>
        private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            string resourceName = Assembly.GetExecutingAssembly().GetName().Name + ".lib." + new AssemblyName(args.Name).Name + ".dll";

            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
            {
                Byte[] assemblyData = new Byte[stream.Length];
                    
                stream.Read(assemblyData, 0, assemblyData.Length);
                    
                return Assembly.Load(assemblyData);
            }
        }
        #endregion
    }
}
