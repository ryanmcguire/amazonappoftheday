﻿// AmazonRequest.cs

using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text;
using HtmlAgilityPack;

namespace AmazonAppOfTheDay
{
    static class AmazonRequest
    {
        #region Public Member Variables
        public const string APP_NAME = "AppName"; // Dictionary Key
        public const string APP_LINK = "AppLink"; // Dictionary Key
        public const string APP_TIME = "AppTime"; // Dictionary Key
        #endregion

        #region Private Member Variables
        private const string HOST = "https://www.amazon.com/";
        private const string APP_STORE_NODE = "/mobile-apps/b?node=2350149011"; // FAD Page
        private const string SIGN_IN_PAGE = "/gp/sign-in.html";
        private static string m_PostActionURL = null;
        private static string m_PostData = null;
        private static bool m_PriceValueIsZero = false;
        private static CookieContainer m_CookieContainer = null;
        private static string m_LastError = null;
        private static Dictionary<string, string> m_LastAppPurchase = new Dictionary<string, string>();
        #endregion

        #region Constructors
        static AmazonRequest()
        {
            HtmlAgilityPack.HtmlNode.ElementsFlags.Remove("form"); // Make sure form <input> data can be parsed
        }
        #endregion

        #region Private Member Functions
        private static string GetAmazonURL(string relativeURL)
        {
            Uri baseURI = new Uri(HOST);
            Uri amazonURI = new Uri(baseURI, relativeURL);
            return amazonURI.ToString();
        }
        #endregion

        #region Public Member Functions
        public static bool AttemptFreeAppPurchase(string email, string password)
        {
            m_CookieContainer = new CookieContainer();
            HtmlAgilityPack.HtmlDocument page = GetWebPage(GetAmazonURL(SIGN_IN_PAGE)); // GET login page
            if (page != null)
            {
                if (RetrieveLoginData(page)) // Set cookies & get new login URL
                {
                    page = GetWebPage(GetAmazonURL(m_PostActionURL)); // GET new login page
                    if (page != null)
                    {
                        if (RetrieveLoginData(page, email, password)) // Retrieve login data
                        {
                            if (MakeWebRequest(GetAmazonURL(m_PostActionURL), m_PostData, WebRequestMethods.Http.Post) != null) // POST login
                            {
                                page = GetWebPage(GetAmazonURL(APP_STORE_NODE)); // GET FAD page
                                if (page != null)
                                {
                                    if (RetrievePurchaseRequestData(page) && m_PriceValueIsZero) // Retrieve FAD Info
                                    {
                                        MakeWebRequest(GetAmazonURL(m_PostActionURL), m_PostData, WebRequestMethods.Http.Post);
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return false;
        }
        public static string GetLastPurchaseData(string key)
        {
            string result = null;
            m_LastAppPurchase.TryGetValue(key, out result);

            return result;
        }
        public static string GetLastError()
        {
            return m_LastError;
        }
        public static void ClearLastError()
        {
            m_LastError = null;
        }
        #endregion

        #region Amazon WebPage Parsing Functions
        private static bool RetrieveLoginData(HtmlAgilityPack.HtmlDocument page)
        {
            m_PostActionURL = null; // Clear

            if (page != null)
            {
                HtmlAgilityPack.HtmlNode formNode = page.DocumentNode.SelectSingleNode("//form[@name='sign-in']");
                if (formNode != null)
                {
                    m_PostActionURL = formNode.GetAttributeValue("action", null);
                }
            }

            return m_PostActionURL != null;
        }

        private static bool RetrieveLoginData(HtmlAgilityPack.HtmlDocument page, string email, string password)
        {
            m_PostData = null; // Clear

            if (page != null)
            {
                HtmlAgilityPack.HtmlNode formNode = page.DocumentNode.SelectSingleNode("//form[@name='sign-in']");
                if (formNode != null)
                {
                    m_PostActionURL = formNode.GetAttributeValue("action", null);
                    IEnumerable<HtmlAgilityPack.HtmlNode> inputCollection = formNode.SelectNodes(".//input");

                    foreach (HtmlAgilityPack.HtmlNode input in inputCollection)
                    {
                        string name = input.GetAttributeValue("name", String.Empty);
                        string value = input.GetAttributeValue("value", String.Empty);

                        if (name.Equals("email"))
                        {
                            value = email;
                        }
                        else if (name.Equals("password"))
                        {
                            value = password;
                        }
                        else if (name.Equals("action") && value.Equals("new-user"))
                        {
                            name = String.Empty; // We are "sign-in" aka "returningCust", not "new-user" aka "newCust", so remove this input
                        }

                        if (input.GetAttributeValue("image", String.Empty).Equals("image")) // Default Coordinate Values for Image Click Submission
                        {
                            if (!String.IsNullOrEmpty(m_PostData))
                            {
                                m_PostData += "&"; // Add '&' if not first datum
                            }
                            m_PostData += "x=0&y=0"; // Apend new data to string
                        }
                        else if (!name.Equals(String.Empty)) // Don't use inputs with empty string for name
                        {
                            if (!String.IsNullOrEmpty(m_PostData))
                            {
                                m_PostData += "&"; // Add '&' if not first datum
                            }
                            m_PostData += name + "=" + value; // Apend new data to string
                        }
                    }

                    return true;
                }
            }

            return false;
        }

        private static bool RetrievePurchaseRequestData(HtmlAgilityPack.HtmlDocument page)
        {
            m_PostActionURL = null; // Clear
            m_PostData = null;

            if (page.DocumentNode != null)
            {
                HtmlAgilityPack.HtmlNode formNode = page.GetElementbyId("handleBuy");
                HtmlAgilityPack.HtmlNode buyButtonNode = page.DocumentNode.SelectSingleNode("//div[@class=\"fad-widget-buy-button\"]"); //<div class="fad-widget-buy-button">

                if (formNode != null && buyButtonNode != null)
                {
                    m_PostActionURL = formNode.GetAttributeValue("action", null);
                    IEnumerable<HtmlAgilityPack.HtmlNode> inputCollection = buyButtonNode.SelectNodes(".//input");

                    foreach (HtmlAgilityPack.HtmlNode input in inputCollection)
                    {
                        string name = input.GetAttributeValue("name", String.Empty);
                        string value = input.GetAttributeValue("value", String.Empty);
                        
                        if (name.Equals("priceValue"))
                        {
                            m_PriceValueIsZero = value.Equals("0"); // Check if Free App costs $0.00
                        }
                        
                        if (input.GetAttributeValue("image", String.Empty).Equals("image")) // Coordinate Values for Image Click Submission
                        {
                            if (!String.IsNullOrEmpty(m_PostData))
                            {
                                m_PostData += "&"; // Add '&' if not first datum
                            }
                            m_PostData += "x=0&y=0"; // Apend new data to string
                        }
                        else if (!name.Equals(String.Empty))
                        {
                            if (!String.IsNullOrEmpty(m_PostData))
                            {
                                m_PostData += "&"; // Add '&' if not first datum
                            }
                            m_PostData += name + "=" + value; // Apend new data to string
                        }
                    }

                    return true;
                }
            }

            return false;
        }
        #endregion

        #region HttpWebRequest Functions
        private static string MakeWebRequest(string url, string data, string method)
        {
            string responseStream = null;

            if (!String.IsNullOrEmpty(url) && !String.IsNullOrEmpty(method))
            {
                HttpWebRequest wReq = (HttpWebRequest)WebRequest.Create(url);
                AddWebRequestHeaders(wReq, method);

                if (data != null && data.Length != 0) // Add Input Data to Request Stream
                {
                    wReq.ContentLength = data.Length;
                    using (Stream stream = wReq.GetRequestStream())
                    {
                        stream.Write(Encoding.UTF8.GetBytes(data), 0, data.Length);
                    }
                }

                try
                {
                    using (HttpWebResponse response = (HttpWebResponse)wReq.GetResponse())
                    {
                        if ((int)response.StatusCode < 299 && (int)response.StatusCode >= 200) // OK Success
                        {
                            using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(response.CharacterSet)))
                            {
                                responseStream = reader.ReadToEnd();
                            }
                        }
                    }
                }
                catch (WebException)
                {

                }
                catch (ArgumentException)
                {

                }
            }

            return responseStream;
        }

        private static void AddWebRequestHeaders(HttpWebRequest request, string method)
        {
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            request.AllowAutoRedirect = true;
            request.AutomaticDecompression = DecompressionMethods.GZip;
            request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate,sdch");
            request.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-US,en;q=0.8");
            request.Method = method;
            if (method.Equals(WebRequestMethods.Http.Post))
            {
                request.ContentType = "application/x-www-form-urlencoded";
            }
            request.CookieContainer = m_CookieContainer;
            request.Credentials = CredentialCache.DefaultNetworkCredentials;
            request.KeepAlive = true;
            //request.Proxy = new WebProxy("http://127.0.0.1:8888", false); // Fiddler, otherwise use default System Proxy
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36";
            request.Host = "www.amazon.com";
        }

        private static HtmlAgilityPack.HtmlDocument GetWebPage(string url)
        {
            HtmlAgilityPack.HtmlDocument htmlDocument = null;

            string htmlData = MakeWebRequest(url, null, WebRequestMethods.Http.Get);
            if (htmlData != null)
            {
                //Console.WriteLine(htmlData); // Test Print Page
                htmlDocument = new HtmlAgilityPack.HtmlDocument();
                htmlDocument.LoadHtml(htmlData);
            }

            return htmlDocument;
        }
        #endregion
    }
}
